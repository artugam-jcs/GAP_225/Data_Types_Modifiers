# Data_Types_Modifiers

WIP

## Integer Types

| Value description                     | Value      | Integer type that fits most closely |
|---------------------------------------|------------|-------------------------------------|
| Highest score ever in Donkey Kong     | 1,260,700  | int                                 |
| Highest score ever in Pac-Man         | 3,333,360  | int                                 |
| Highest game points ever in Asteroids | 41,336,440 | int                                 |
| Highest score possible in bowling     | 450        | short                               |
| A number in the range [-100, 100]     | 10         | byte                                |
| A number in the range [-1000, 1000]   | 632        | short                               |

