#include <assert.h>
#include <iostream>
#include <stdlib.h>
#include <string>
#include <conio.h>

//////////////////////////////////////////////
// Assertion
//////////////////////////////////////////////

template<typename T>
bool InRange(T min, T max, T val)
{
	return min <= val && val <= max;
}

int Divide(int left, int right)
{
	assert(right != 0);
	return left / right;
}


int GetIndexInGrid(int x, int y, int width)
{
	assert(InRange(0, 100, x) && InRange(0, 100, y));
	return x + y * width;
}

//void SquadLeader::OnDeath()
//{
//	assert(g_pSquadLeader != nullptr, "Variable g_pSquadLeader cannot be nullptr");
//	g_pSquadLeader->OnSquadMemberDeath(this);
//}

int GetNumberFromUser(int min, int max)
{
	assert(min > max);

	std::cout << "Enter a number between " << min << " and " << max << '\n';

	int response;

	std::cin >> response;

	// NOTE: response return 0 when invalid string is inputted.
	// However, 0 is still a valid response; therefore, assert macro here is meaningless.
	assert(response == 0);

	return response;
}

//////////////////////////////////////////////
// Integer Types
//////////////////////////////////////////////

void IntegerTypes5()
{
	// | Value description                     | Value      | Integer type that fits most closely |
	// |---------------------------------------|------------|-------------------------------------|
	// | Highest score ever in Donkey Kong     | 1,260,700  | int                                 |
	// | Highest score ever in Pac-Man         | 3,333,360  | int                                 |
	// | Highest game points ever in Asteroids | 41,336,440 | int                                 |
	// | Highest score possible in bowling     | 450        | short                               |
	// | A number in the range [-100, 100]     | 10         | byte                                |
	// | A number in the range [-1000, 1000]   | 632        | short                               |
}

void IntegerTypes6()
{
	char a6 = UINT_MAX;
	short b6 = INT_MIN;
	int32_t c6 = a6 + b6;
	std::cout << c6 << std::endl;
}

void CastingPrimitives7()
{
	float f = 3.14159f;
	int i = f;  // Warning	C4244	'initializing': conversion from 'float' to 'int'
	std::cout << i << std::endl;
}

//////////////////////////////////////////////
// Casting Primitives
//////////////////////////////////////////////

void CastingPrimitives8()
{
	float f = 3.14159f;
	int i = (int)f;  // No warning
	std::cout << i << std::endl;
}

void CastingPrimitives9()
{
	float f = 3.14159f;
	int i = static_cast<int>(f);  // No warning
	std::cout << i << std::endl;
}

void CastingPrimitives10()
{
	// static_cast is better since it gives you a compile time checking ability.
}

//////////////////////////////////////////////
// Casting Objects
//////////////////////////////////////////////

// 11
class Animal
{
public:
	std::string name;

	Animal(std::string name)
	{
		this->name = name;
	}

	virtual void PrintName()
	{
		std::cout << "Name: " << name << std::endl;
	}
};

// 12
class Cat : public Animal
{
public:
	int age;

	Cat(std::string name)
		: Animal(name)
		, age(0)
	{
		// empty..
	}

	void PrintName() override
	{
		std::cout << "Name: " << name << std::endl;
	}

	void PrintAge()
	{
		std::cout << "Age: " << age << std::endl;
	}
};

// 13
class Dog : public Animal
{
public:
	float pi;

	Dog(std::string name)
		: Animal(name)
		, pi(3.14f)
	{
		// empty..
	}

	void PrintName() override
	{
		std::cout << "Name: " << name << std::endl;
	}

	void PrintPi()
	{
		std::cout << "Pi: " << pi << std::endl;
	}
};

// 14
void CastingObjects14()
{
	Animal* pAnimal = new Cat("Kitty");

	// 15
	Dog* pDog = static_cast<Dog*>(pAnimal);
	// 15.a: It does succeed since the is a runtime error; therefore, compiler cannot
	// acknowledge this before the program starts.

	// Didn't know this will work. I guess this is a implicit cast.
	if (pDog)
	{
		pDog->PrintName();
		pDog->PrintPi();    // Variable pi doesn't get initialize!
	}

	// 17
	Animal* pAnimal2 = dynamic_cast<Animal*>(pDog);

	// 17.a pAnimal2 is a nullptr
	std::cout << (pAnimal2 == nullptr) << std::endl;

	// 18
	Dog* pDog2 = dynamic_cast<Dog*>(pAnimal);

	delete pAnimal;
}

// 22. From my obversation, it seems like static cast has less ristriction
// and leave everything else uninitialized. Unlike dynamic cast, if the cast
// failed, it will just return nullptr.

//////////////////////////////////////////////
// Type Aliases
//////////////////////////////////////////////

template<typename T>
T rand(T min, T max) //Pass in range
{
	return rand() % max + min;
}

typedef Animal** AnimalArray;

void TypeAliases23()
{
	const size_t kSize = 10;
	AnimalArray ppAnimals = new Animal*[kSize];

	for (size_t index = 0; index < kSize; ++index)
	{
		int choice = rand(0, 2);

		Animal* pAnimal = nullptr;

		switch (choice)
		{
		case 0:
		{
			pAnimal = new Cat("Lovy");
		}
		break;
		case 1:
		{
			pAnimal = new Dog("Guggi");
		}
		break;
		}

		ppAnimals[index] = pAnimal;
	}

	delete[] ppAnimals;
}

//////////////////////////////////////////////
// Enumerations
//////////////////////////////////////////////

enum Key
{
	k_W,
	k_A,
	k_S,
	k_D,
	k_Q,
};

Key Convert(int key)
{
	switch (key)
	{
	case static_cast<int>('w'): return Key::k_W;
	case static_cast<int>('a'): return Key::k_A;
	case static_cast<int>('s'): return Key::k_S;
	case static_cast< int>('d'): return Key::k_D;
	}
	return Key::k_Q;
}

void Enumerations28()
{
	std::cout << "Etner keys: w, a, s, d or q: ";
	char input = _getch();

	Key key = Convert(input);

	std::cout << "\nYou chose: " << key << std::endl;
}

int main()
{
	srand(time(NULL));

	//Divide(1, 2);
	//GetIndexInGrid(50, 15, 10);

	//IntegerTypes6();
	//CastingObjects14();
	//TypeAliases23();
	Enumerations28();

	system("pause");

	return 0;
}
